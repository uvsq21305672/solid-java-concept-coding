
public abstract class Personnel {
	
	public int salaire;
	public int ancienete;
	public int base = 1500;
	
	public Personnel(int salaire, int ancienete) {
		this.salaire = salaire;
		this.ancienete = ancienete;
	}
	
	public abstract int CalculSalaire();
}
