
public class Vendeur extends Personnel {

	private int commission;
	
	public Vendeur(int com, int salaire, int ancienete) {
		super(ancienete, salaire);
		this.commission=com;
	}
	
	public int CalculSalaire() {
		return salaire = (base + 20 * ancienete) + commission;
	}
}
