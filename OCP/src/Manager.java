
public class Manager extends Personnel{
	
	private int nbrEmploye;
	
	public Manager(int nbr, int salaire, int ancienete) {
		super(ancienete, salaire);
		this.nbrEmploye=nbr;
	}
	
	public int CalculSalaire() {
		return salaire = (base + 20 * ancienete) + (100 * nbrEmploye);
	}
}
