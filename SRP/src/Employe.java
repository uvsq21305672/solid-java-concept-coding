
public class Employe {

		private final String nom;
		private final String adresse;
		
		public Employe(String nom, String adresse){
			
	        this.nom = nom;
	        this.adresse = adresse;
	    }
		
		public void afficheCoordonnees() {
			
			System.out.println(nom+ " . "+ adresse);
		}
}
